import vk_api
import pickle
import pandas as pd
from db_utils import sql_execute as query
from datetime import datetime


def auth_handler():
    """ При двухфакторной аутентификации вызывается эта функция.
    """
    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")
    # Если: True - сохранить, False - не сохранять.
    remember_device = True

    return key, remember_device


def give_me_a_power():
    login, password = '+79231117346', 'It_is_not_my_password'  # del
    vk_session = vk_api.VkApi(
        login, password,
        # функция для обработки двухфакторной аутентификации
        auth_handler=auth_handler
    )

    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return

    vk = vk_session.get_api()
    return vk


def get_common_users(vk, offsets, count) -> dict:

    first_group, second_group = list(), list()

    for container, group in zip((first_group, second_group), ('typical_nsk', 'act54')):
        for i in range(offsets):
            members = vk.groups.getMembers(
                offset=i * 1000,
                group_id=group,
                # id, возраст, город, id фото, и скрыт ли профиль
                fields=["id", "bdate", "city", "photo_id", "is_closed"],
                count=count
            )
            container.extend(members['items'])

    first_dict = {d["id"]: d for d in first_group}
    second_dict = ({d["id"]: d for d in second_group})

    common_users = first_dict.keys() & second_dict.keys()
    common_dict = {key: first_dict[key] for key in common_users}

    print(f"Common users are {len(common_dict)} members")
    return common_dict


def get_age_from_bdate(bdate: str):
    if bdate == "" or bdate.count('.') == 1:
        return datetime.now().year + 1
    else:
        return int(bdate.split('.')[-1])


def create_age_feature(users: dict):
    for key in users.keys():
        age = get_age_from_bdate(users[key].get('bdate', ""))
        users[key]['age'] = datetime.now().year - age
        users[key].pop("bdate", None)


def create_and_save_id_age_table(users: dict):
    id_age_dict = {key: users[key]['age'] for key in users.keys()}
    # print(id_age_dict)
    with open('data/id_age.pickle', 'wb') as handle:
        pickle.dump(id_age_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)


def data_cleaning(users: dict):
    cleaned_dict = dict()

    for key in users.keys():
        user: dict = users[key]
        # id, возраст, город, id фото, и скрыт ли профиль
        age = user["age"]
        city = user.get('city', {"title": ""})["title"]
        photo_id = user.get('photo_id', "")
        is_closed = user.get('is_closed', bool())
        cleaned_dict[key] = {"age": age, "city": city,
                             "photo_id": photo_id, "is_closed": is_closed
                             }
    return cleaned_dict


def save_dict_to_csv(users_dict, file_name):
    df = pd.DataFrame.from_dict(users_dict, orient='index').reset_index()
    df.columns = ['id', 'age', 'city', 'photo_id', 'is_closed']
    df.to_csv(file_name, encoding="utf-8", index=False)


def sql_handle(file_path, table_name, param_dict):
    create = f"""
    CREATE TABLE {table_name} (
        id SERIAL NOT NULL,
        age INTEGER,
        city VARCHAR(128), 
        photo_id VARCHAR(128), 
        is_closed BOOLEAN
    );
    """

    copy = f"""
    COPY {table_name}(
        id, 
        age, 
        city, 
        photo_id, 
        is_closed
    ) 
    FROM '{file_path}' DELIMITER ',' CSV HEADER;
    """

    drop = f"""
    DROP TABLE {table_name};
    """
    # query(drop, param_dict)
    query(create, param_dict)
    query(copy, param_dict)


def main(offsets, count, con_params):
    vk = give_me_a_power()
    common_users = get_common_users(vk, offsets=offsets, count=count)
    create_age_feature(common_users)

    create_and_save_id_age_table(common_users)
    clean_data = data_cleaning(common_users)

    path = "C:\\cmder\\api_seminar\\data\\final_table.csv"
    save_dict_to_csv(clean_data, path)
    sql_handle(path, "final_table", con_params)


if __name__ == '__main__':
    params = {
        'database': 'api_seminar',
        'host': 'localhost',
        'user': 'postgres',
        'password': 'password'
    }
    main(50, 1000, params)
