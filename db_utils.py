import psycopg2
from psycopg2.extras import RealDictCursor


def sql_execute(sql_give, params):
    conn = psycopg2.connect(**params)
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    answer = None

    # print(sql_give)
    cursor.execute(sql_give)
    conn.commit()
    try:
        answer = cursor.fetchall()
    except:
        pass
    finally:
        conn.close()
        cursor.close()
        return answer
