# Задача
1. Взять любые две группы вк с аудиторией более миллиона (или где точно будут совпадения) и найти людей, которые состоят в обеих. Затем найти их средний возраст
результат код на гитлаб с pkl файлом, где забиты все этим люди.

2. Следующее задание - будет записать значения всех пользователей, которые состоят в обеих, в БД постгрес 
поля - id, возраст, город,  id фото, и скрыт ли профиль

___
# Интсрукция

1. pip install requirements.txt 

2. install postgresql, create DB api_seminar 

3. run main.py  (change connect params)
___
# Результат
1. [Итоговый py файл](https://gitlab.com/Kum4yk/api_seminar/-/blob/master/main.py)

2. [pkl file](https://gitlab.com/Kum4yk/api_seminar/-/blob/master/data/id_age.pickle)

3. [Тетрадь с проверкой записи в БД](https://gitlab.com/Kum4yk/api_seminar/-/blob/master/check_result.ipynb)